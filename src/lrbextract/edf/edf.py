import logging
import shutil
from pathlib import Path

import pandas as pd
from dwclib.common.db import pguri
from sqlalchemy import MetaData, Table, create_engine, select

from lrbextract.config import configuration


def build_edf_tasks(patients, config, outroot):
    logger = logging.getLogger("default")

    if "edf" not in config["extract"]:
        return []
    mountpoint = configuration["nas_mountpoint"]
    if not mountpoint:
        logger.warning("NAS mountpoint not defined. EDFs will not be copied!")
        return []
    mountpoint = Path(mountpoint)
    tasks = []
    for p in patients:
        dwcid = p.name
        ipp = p["lifetimeid"]
        if not ipp:
            continue
        edfdir = outroot / str(dwcid) / "edf"
        edfdir.mkdir(parents=True, exist_ok=True)
        files = query_edf_files(ipp)
        tasks.append((mountpoint, files, edfdir))
    return [(extract_edf_files, part) for part in tasks]


def query_edf_files(ipp):
    logger = logging.getLogger("default")

    engine = create_engine(pguri.set(database="lrbdata"))
    pgmeta = MetaData(schema="public")
    pgmeta.reflect(engine)

    t_edf = Table("edfmeta", pgmeta, autoload_with=engine)
    edft = t_edf.c

    with engine.connect() as conn:
        q = select(edft.folder, edft.filename)
        q = q.where(edft.lifetimenumbers.contains([ipp]))
        logger.debug(q.compile(engine))
        df = pd.read_sql(q, conn)
        files = [Path(d) / f for _, (d, f) in df.iterrows()]
        logger.debug(files)
    return files


def extract_edf_files(mountpoint, src_files, outfolder):
    for f in src_files:
        shutil.copy(mountpoint / f, outfolder)
