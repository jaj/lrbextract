from math import ceil
from pathlib import Path
from typing import List, Optional

import pandas as pd
from dwclib import read_numerics


def get_numerics(
    dwcid: str,
    timedelta: pd.Timedelta,
    outfolder: Path,
    data_begin: pd.Timestamp,
    data_end: pd.Timestamp,
    labels: Optional[List[str]] = None,
):
    patientdir = outfolder / str(dwcid)
    patientdir.mkdir(exist_ok=True)
    numdir = patientdir / "dwcnumerics"
    numdir.mkdir(exist_ok=True)

    if data_begin is None or data_end is None:
        return []

    npartitions = int(ceil((data_end - data_begin) / timedelta))

    params = []
    for i in range(npartitions):
        beg_time = data_begin + i * timedelta
        end_time = beg_time + timedelta
        params.append(
            (read_numerics, "dwcnumerics", dwcid, numdir, beg_time, end_time, labels)
        )
    return params
