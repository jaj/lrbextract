def to_parquet(df, outfile):
    df.to_parquet(
        outfile,
        coerce_timestamps="ms",
        allow_truncated_timestamps=True,
        compression="GZIP",
    )
