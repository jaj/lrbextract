import logging

import pandas as pd
from dwclib.common.db import pguri
from sqlalchemy import MetaData, Table, create_engine, select

from lrbextract.utils import to_parquet


def build_syringes_tasks(patients, config, outroot):
    if "syringes" not in config["extract"]:
        return []
    tasks = []
    labels = []  # TODO
    for p in patients:
        dwcid = p.name
        bedlabel = p["bedlabel"]
        syringesdir = outroot / str(dwcid) / "syringes"
        syringesdir.mkdir(parents=True, exist_ok=True)
        tasks.append((bedlabel, p.data_begin, p.data_end, syringesdir, labels))
    return [(syringes_extract, part) for part in tasks]


def syringes_extract(bedlabel, data_begin, data_end, outfolder, labels):
    logger = logging.getLogger("default")

    engine = create_engine(pguri.set(database="lrbdata"))
    pgmeta = MetaData(schema="kafka")
    pgmeta.reflect(engine)

    t_syringes = Table("syringes", pgmeta, autoload_with=engine)
    syrt = t_syringes.c

    outfile = outfolder / "syringes.parquet"
    outfile_placeholder = outfolder / f".{outfile.name}.empty"

    with engine.connect() as conn:
        q = select(
            syrt.datetime,
            syrt.drug,
            syrt.rate,
            syrt.volume,
            syrt.clinicalunit,
            syrt.bedlabel,
        )
        q = q.where(syrt.datetime >= data_begin)
        q = q.where(syrt.datetime <= data_end)
        q = q.where(syrt.bedlabel == bedlabel)
        if labels:
            q = q.where(syrt.drug in labels)
        logger.debug(q.compile(engine))
        df = pd.read_sql(q, conn, index_col="datetime")

    if len(df) < 1:
        outfile_placeholder.touch()
        return
    df = df.pivot_table(index="datetime", columns="drug", values=["rate", "volume"])
    df.columns = df.columns.map("{0[1]}-{0[0]}".format)
    to_parquet(df, outfile)
