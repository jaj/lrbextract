import logging
from typing import List, Optional, Union

import pandas as pd
from dwclib import read_patients
from dwclib.common.db import dwcuri, pguri
from dwclib.patients import read_patients_dwc_native
from pandas.api.types import is_list_like
from sqlalchemy import MetaData, Table, and_, create_engine, select


def search_dwclabels(
    numericlabels: Optional[List[Union[List[str], str]]] = None,
    wavelabels: Optional[List[Union[List[str], str]]] = None,
) -> pd.DataFrame:
    numcrit = []
    wavcrit = []
    logger = logging.getLogger("default")

    engine = create_engine(pguri)

    pgmeta = MetaData(bind=engine)
    t_patients = Table("patients", pgmeta, autoload_with=engine)
    p = t_patients.c
    t_patientlabels = Table("patientlabels", pgmeta, autoload_with=engine)
    pl = t_patientlabels.c

    if numericlabels is not None:
        for n in numericlabels:
            if is_list_like(n):
                numcrit.append(pl.numericlabels.overlap(n))
            else:
                numcrit.append(pl.numericlabels.contains([n]))

    if wavelabels is not None:
        for n in wavelabels:
            if is_list_like(n):
                wavcrit.append(pl.wavelabels.overlap(n))
            else:
                wavcrit.append(pl.wavelabels.contains([n]))

    q = select(t_patients, pl.numericlabels, pl.wavelabels)
    q = q.select_from(
        t_patients.join(t_patientlabels, pl.patientid == p.patientid, isouter=True)
    )
    q = q.where(and_(*numcrit))
    q = q.where(and_(*wavcrit))
    q = q.where(p.dbname == dwcuri.database)

    with engine.connect() as conn:
        logger.debug(q.compile(engine))
        df = pd.read_sql(q, conn, index_col="patientid")
    return [r for _, r in df.iterrows()]


def search_dwcpatients(df_search_crit):
    results = []
    logger = logging.getLogger("default")
    for _, row in df_search_crit.iterrows():
        row = row.dropna()
        try:
            res = read_patients(**row)
        except Exception as e:
            logger.warning(e)
            continue
        if res is not None:
            results += [r for _, r in res.iterrows()]
    return results


def search_dwcpatients_native(df_search_crit):
    results = []
    logger = logging.getLogger("default")
    for _, row in df_search_crit.iterrows():
        row = row.dropna()
        try:
            res = read_patients_dwc_native(**row)
        except Exception as e:
            logger.warning(e)
            continue
        if len(res) > 0:
            results += [r for _, r in res.iterrows()]
    return results
