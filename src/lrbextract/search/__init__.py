import pandas as pd

from .dwc import search_dwclabels, search_dwcpatients, search_dwcpatients_native


def search(config, cache):
    search_labels_ = cache.cache(search_labels)
    search_dwcpatients_ = cache.cache(search_dwcpatients)
    search_dwcpatients_native_ = cache.cache(search_dwcpatients_native)
    results = []
    for search_crit in config:
        if "dwcpatients" in search_crit:
            df_search_crit = pd.read_csv(search_crit["dwcpatients"], dtype="string")
            results += search_dwcpatients_(df_search_crit)
        if "dwcpatients_native" in search_crit:
            df_search_crit = pd.read_csv(
                search_crit["dwcpatients_native"], dtype="string"
            )
            results += search_dwcpatients_native_(df_search_crit)
        elif "dwclabels" in search_crit:
            results += search_labels_(search_crit["dwclabels"])
        else:
            continue
    return results


def search_labels(config):
    numericlabels = config["dwcnumerics"] if "dwcnumerics" in config else None
    wavelabels = config["dwcwaves"] if "dwcwaves" in config else None
    return search_dwclabels(numericlabels, wavelabels)
