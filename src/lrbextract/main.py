import concurrent.futures
import logging
from contextlib import suppress
from multiprocessing import cpu_count
from pathlib import Path
from typing import IO

import click
import pandas as pd
import yaml
from joblib import Memory

from lrbextract.dwc.build_tasks import build_dwc_tasks
from lrbextract.edf.edf import build_edf_tasks
from lrbextract.lab.labgds import build_labgds_tasks
from lrbextract.search import search
from lrbextract.syringes.syringes import build_syringes_tasks

logger = logging.getLogger("default")


@click.command()
@click.option(
    "--config-file", type=click.File("r"), help="Configuration file.", required=True
)
@click.option(
    "--outfolder",
    required=True,
    help="Directory to which we write output files.",
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        path_type=Path,
    ),
)
@click.option(
    "--cleanup",
    is_flag=True,
    show_default=True,
    default=True,
    help="Clean up temporary files.",
)
@click.option(
    "--debug",
    is_flag=True,
    show_default=True,
    default=False,
    help="Show debug messages.",
)
def main(config_file: IO[str], outfolder: Path, cleanup: bool, debug: bool):
    loglevel = logging.DEBUG if debug else logging.INFO
    logger.setLevel(loglevel)
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    logger.addHandler(ch)

    config = yaml.safe_load(config_file)
    logger.debug(config)

    with suppress(KeyError):
        logfile = config["logfile"]
        fh = logging.FileHandler(logfile)
        logger.addHandler(fh)

    return run_extraction(config, outfolder, cleanup, debug)


def run_extraction(config: dict, outfolder: Path, cleanup: bool, debug: bool):
    outroot = outfolder / config["project"]
    outroot.mkdir(exist_ok=True)
    dataroot = outroot / "data"
    dataroot.mkdir(exist_ok=True)

    # joblib dependency only for this caching functionality
    cache = Memory(outroot / "cache")

    patients = []
    if "search" in config:
        patients = search(config["search"], cache)
        logger.info(f"Found {len(patients)} patients.")

    patient_outfile = outroot / "patients.csv"
    logger.info(f"Writing patients to {patient_outfile}.")
    patientdf = pd.DataFrame(patients)
    patientdf.to_csv(patient_outfile, index_label="dwcid")

    logger.info("Building list of tasks.")
    tasks = []
    tasks += build_dwc_tasks(patients, config, dataroot)
    tasks += build_labgds_tasks(patients, config, dataroot)
    tasks += build_syringes_tasks(patients, config, dataroot)
    tasks += build_edf_tasks(patients, config, dataroot)
    logger.debug(tasks)

    nworkers = min(10, cpu_count() - 1)
    with suppress(KeyError, ValueError):
        nworkers = int(config["nworkers"])

    if debug:
        logger.info("Starting in single threaded debug mode.")
        # Single threaded version
        for f, params in tasks:
            f(*params)
    else:
        # Multi-process version
        logger.info(f"Starting tasks with {nworkers} workers.")
        with concurrent.futures.ProcessPoolExecutor(max_workers=nworkers) as executor:
            future_to_params = {
                executor.submit(f, *params): params for f, params in tasks
            }
            for future in concurrent.futures.as_completed(future_to_params):
                p = future_to_params[future]
                exc = future.exception()
                if exc:
                    logging.error(f"Exception {exc} raised for: {p}")

    if cleanup:
        logger.info("Cleaning up.")
        tempfiles = dataroot.glob("**/.*.empty")
        for f in tempfiles:
            f.unlink()


if __name__ == "__main__":
    main()
